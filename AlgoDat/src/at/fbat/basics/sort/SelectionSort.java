package at.fbat.basics.sort;

import at.fbat.basics.DataGenerator;

public class SelectionSort implements Sorter{

	@Override
	public int[] sort(int[] unsorted) {
		//int CurrentPosition = 0;
		//int CurrentSmallest = 0;
		
		 for (int i = 0; i < unsorted.length - 1; i++)  
	     {  
	         int CurrentPosition = i;
	         for (int j = i + 1; j < unsorted.length; j++){  
	             if (unsorted[j] < unsorted[CurrentPosition]){  
	                 CurrentPosition = j;//searching for lowest index  
	             }  
	         }  
	         int CurrentSmallest = unsorted[CurrentPosition];   
	         unsorted[CurrentPosition] = unsorted[i];  
	         unsorted[i] = CurrentSmallest;  
	     }
		
		
		return unsorted;
	}  
}
