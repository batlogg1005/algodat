package at.fbat.basics.sort;

public interface Sorter {

	public int[] sort(int[] unsorted);
}
