package at.fbat.basics;

import java.util.Random;
import java.util.ArrayList;
import java.util.List;

public class DataGenerator {

	private List<Integer> unsorted = new ArrayList<>();

	public int[] generateDataArray(int size) {
		Random random = new Random();
		size = 4;
		int[] dataArray = new int[size];
		for (int i = 0; i < size; i++) {
			dataArray[i] = random.nextInt(100);

			// System.out.println(dataArray[i]);

		}

		return null;

	}

	public int[] generateDataArrayMinMax(int size, int min, int max) {
		Random random = new Random();
		int[] dataArray = new int[size];
		for (int i = 0; i < size; i++) {
			dataArray[i] = random.nextInt(max - min + 1) + min;

			// System.out.println(dataArray[i]);
		}

		return dataArray;

	}

	public void printArray(int[] data) {
		for (int i = 0; i < data.length; i++) {
			System.out.println(i + ". Wert" + data[i]);
		}
	}

}
