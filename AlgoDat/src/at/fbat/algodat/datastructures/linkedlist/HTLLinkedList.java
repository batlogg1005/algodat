package at.fbat.algodat.datastructures.linkedlist;

public class HTLLinkedList {
	private Node root;
	private int length = 0;
	
	public void add(int value){
		length++;
		if (root == null){
			Node n = new Node(value, null);
			this.root = n;
			return;
		}
		
		Node actNode = root;
		
		while(actNode.next != null){
			actNode = actNode.next;
		}
		
		Node n = new Node(value, null);
		actNode.next = n;
		
	}
	
	public void clear(){
		root = null;
	}
	
	public void remove(int index){
		
		
		
		if (index ==0){
			root = root.next;
		}
		
		if (index==1){
			root.next = root.next.next;
		}
		
		Node active = root.next.next;
		Node preActive = root.next;
		
		for (int i = 2; i < index; i++){
			preActive = preActive.next;
			active = active.next;
		}
		
		preActive.next = active.next;
		active.next = null;

		length--;
	}
	
	
	public void printList(){
		Node active = this.root;
		
		while(active.next!=null){
			System.out.println(active.value);
			active = active.next;
		}
	}
	
	public int get(int index){
		
		Node active = root;
		
		for (int i = 0; i < index+1; i++){
			active = active.next;
		}
		
		return active.value;
	}

	public Node getRoot() {
		return root;
	}

	public int getLength() {
		return length;
	}
	
	
	
	
	
	
	
}
