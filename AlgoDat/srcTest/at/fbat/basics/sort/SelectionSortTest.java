package at.fbat.basics.sort;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import at.fbat.basics.DataGenerator;

public class SelectionSortTest {

	private DataGenerator classUnderTest;

	@Before
	public void setUp() throws Exception {
		classUnderTest = new DataGenerator();
	}

	@Test
	public void testSelectionSort() {
		int[] unsorted = classUnderTest.generateDataArrayMinMax(10, 0, 1000);
		Sorter is = new SelectionSort();
		int[] sorted = is.sort(unsorted);

		for (int i = 0; i < sorted.length - 1; i++) {
			System.out.println(i + ". Zahl: " + sorted[i] + ",");

			if (sorted[i] > sorted[i + 1]) {
				fail("not sorted");
			}
		}
		assertTrue(true);
	}

}
