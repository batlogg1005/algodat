package at.fbat.algodat.datastructures.linkedlist;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class HTLLinkedListTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		HTLLinkedList list = new HTLLinkedList();
		
		list.add(3);
		list.add(4);
		list.add(5);
		list.add(9);
		list.add(53);
		list.add(93);
		list.add(522);
		list.add(9222);
		
		list.printList();
		list.remove(5);
		
		list.printList();
		
		System.out.println(list);
		assertTrue(true);
	}
	
	@Test
	public void testRemove() {
		HTLLinkedList list = new HTLLinkedList();
		
		list.add(11);
		list.add(12);
		list.add(13);
		list.add(14);
		list.add(15);
		list.add(16);
		
		list.printList();
		list.remove(4);
		
		assertEquals(5, list.getLength());
		list.printList();
			
		fail("Not yet implemented");
	}
	
	@Test
	public void testGet() {
		HTLLinkedList list = new HTLLinkedList();
		
		list.add(11);
		list.add(12);
		list.add(13);
		list.add(14);
		list.add(15);
		list.add(16);
		
		list.printList();
		int res = list.get(5);
		assertEquals(16, res);
		//list.printList();
			
	}

}
